(ns clj-workshop.dev-server
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.reload :refer [wrap-reload]]
            [clj-workshop.time-tracker :refer [app]]))

(def dev-handler
  (wrap-reload #'app))

(defn -main [& args]
  (run-jetty dev-handler {:port 13333
                          ;; NOTE(luis) if you call this function from a repl,
                          ;; it would by default block the main thread; this option
                          ;; makes it spawn a separate thread instead.
                          :join? false}))
