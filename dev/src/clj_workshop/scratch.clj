(ns clj-workshop.scratch
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as string]))

;; a good read:
;; https://clojure.org/about/rationale;

;; Basic expressions
(comment
  ;; literals:
  1
  2N
  0x3
  2/4


  \a
  (str \a \b \c \newline)

  nil true false

  ##Inf

    :a-keyword
   :namespaced/keyword
  ::qualified-keyword

  ;; sequences:
  (type '(1 2 3))
  (type [1 2 3])
  ;; keys and values can be anything
  (type {:a 42 :b 43})
  (type #{1 2 #_2 3})
  ;; honorable mention: namespaced maps
  (:task/duration
   #:task{:duration 90 :description "hello"})
  ;; cons/conj/assoc/dissoc/update/first/rest/next

  ;; reader forms:
  #_'(is a comment)
  ;; this is also a comment
  ;; anything preceded by `#` implies a special reader tag
  #'str
  str
  (re-matches #"[a-z]+" "hello")
  (#(* 2 %1 %2) 3 4)

  ;; tagged literals:
  #inst "2018-03-28T10:48:00.000"
  
  ;; function invocation
  (< 1 3 2)
  (:key {:key 42 :other-key 34})

  ;; special forms:
  ;; https://clojure.org/reference/special_forms
  (do (println "hello")
      (* 2 42))
  
  (if true 42  43)
  ((var str) 42 43)

  (let [add2 (fn [x] (+ 2 x))
        fact (fn [n]
               ;; clojure can't do tail recursion optimization, so recursive functions
               ;; are likely to blow the stack, hence the special form recur
               (loop [cnt n acc 1]
                 (if (zero? cnt)
                   acc
                   (recur (dec cnt) (* acc cnt)))))]
    (fact 4))
  

  
  )

;; destructuring

(comment
  (let [[first & rest] [1 2 3 4]
        {:keys [x y] :as m}
        {:x 42
         :y 43
         :z 44}]
    (vector x y first (vals m))))

;; macros and quoting
(comment
  ;; doesn't resolve anything: literally a list
  '(str :quotes "everything")
  (let [x 42
        xs [1 2 3]]
    ;; resolves symbols, unquotes to resolve anything else
    `(str :quotes ~x ~@xs))
  
  (macroexpand
   '(when (even? 42)
      (println "even!")
      42))
  (macroexpand
   '(-> {}
        (assoc :x 42)
        (update-in [:x] inc)
        (identity)))
  (let [x {}]
    (macroexpand
     `(cond-> ~x
        (even? 42)
        (assoc :x 42)
        
        (even? 43)
        (assoc :y 43)
        
        :then identity))))

;; higher order functions
(comment
  (let [add2 #(+ 2 %)
        by2  #(* 2 %)
        composed (comp add2 by2)]
    (composed 4))
  (map (partial + 2) [1 2 3 4])
  (map (juxt (partial + 2)
             (partial * 2))
       [1 2 3 4])
  (map (complement even?)
       [1 2 3 4])
  (map (constantly 42)
       [1 2 3 4])
  (let [printb4 (fn [f]
                  (fn [x]
                    (println "hello!" x)
                    (f x)))
        pure-f (partial + 2)
        impure-f (printb4 pure-f)]
    (impure-f 42)))


;; java interop 

(comment
  (doto (new java.util.HashMap)
    (.put "x" 42)
    (.put "y" 43)))

;; protocols

(defprotocol IShow
  (show [this] "Prints an x")
  (show-str [this] "Turns an x into a display-able string"))

(deftype Person [name age]
  IShow
  (show [_] (println name " is " age))
  (show-str [_] (str name " is " age)))

(extend-protocol IShow
  Long
  (show [i] (println i))
  (show-str [i] (str i)))

(comment
  (show-str (Person. "Luis" 33))
  (show-str 42))


;; specs
;; https://clojure.org/guides/spec

(s/def ::non-blank-string (s/and string?
                                 (complement string/blank?)))

(comment
  (s/valid? ::non-blank-string "")
  (s/conform even? 43)
  (s/explain ::non-blank-string ""))

(s/def ::x int?)
(s/def ::y string?)

(s/def ::my-map (s/keys :req-un [::x]
                        :opt-un [::y]))

(s/def ::id-or-key (s/or :id int?
                        :key string?))


(comment
  (s/valid? ::my-map {:x 42})
  (s/valid? ::my-map {:x :hello})
  (s/valid? ::my-map {:x 42
                      :y "hello"})
  
  (s/conform ::id-or-key 42))
;;;
;; ADVANCED:
;;; 

;; datatypes:
;; https://clojure.org/reference/datatypes

;; transients:
;; https://clojure.org/reference/transients

;; transducers:
;; https://clojure.org/reference/transducers

;; multimethods:
;; https://clojure.org/reference/multimethods

;; protocols:
;; https://clojure.org/reference/protocols

;; concurrent access:
;; refs: https://clojure.org/reference/refs
;; agents: https://clojure.org/reference/agents
;; atoms: https://clojure.org/reference/atoms

;; reducers:
;; https://clojure.org/reference/reducers
