(ns clj-workshop.time-tracker
  (:require [ring.adapter.jetty :refer [run-jetty]])
  (:gen-class))

(defn app [request]
  {:status 200
   :headers {"Content-Type" "text/plan; charset=UTF-8"}
   :body "Hello, world\n"})

(defn -main [& args]
  (run-jetty app {:port 3333}))
